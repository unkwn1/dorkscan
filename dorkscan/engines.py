from .network import new_user_agent
from .dorktypes import SearchEngine
import requests
import json


class Engines:
    # TODO: export function, add function
    def __init__(self, fp: str = None) -> None:
        """Initializes with default dictionary objects Bing, Ask,Wow.

        Defaults pulled from: [GitLab snippet](https://gitlab.com/unkwn1/dorkscan/-/snippets/2229059/raw/main/searchengines.json)

        Optional Args:
            `fp`: str
                full or relative path to a JSON file containing SearchEngine objects
        """
        if fp != None:
            pass
        with requests.get(
            "https://gitlab.com/unkwn1/dorkscan/-/snippets/2229059/raw/main/searchengines.json"
        ) as raw:
            self.search_engines = json.loads(raw.text)

    def load_engine(self, engine: str) -> SearchEngine | ValueError:
        """Loads a specified SearchEngine from user input.

        Required Args:
            `engine: str`
                name of a supported engine

        Refer to utils.types to add your own SearchEngine dict.
        """
        engine = engine.upper()
        if engine in self.search_engines.keys():
            return self.search_engines[engine]
        else:
            raise KeyError(f"{engine} is not supported")
